import 'package:flutter/material.dart';

class UiSet with ChangeNotifier {
  static double _fontsize = 0.5;

  set fontSize(newValue) {
    _fontsize = newValue;
    notifyListeners();
  }

  double get fontSize => _fontsize * 50;
  double get sliderfontsize => _fontsize;
}
